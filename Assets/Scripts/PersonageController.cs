﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;
public class PersonageController : MonoBehaviour {
    public Transform[] lanes;
    int current = 2;
    Animator anim;
  //  public SkeletonWrapper sw;
    public BodySourceManager bodySourceManager;
    bool transition = false;
    bool dead = false;
    ObstaclesController obsContr;
    public FailureController fc;
    int lives = 5;
    public bool[] checkedLanes;
    GameParams gameParams = new GameParams();

    bool firstLaunch = true;
    public GameObject countDown;
    float previousR = 0f;
    Body currentBody;
    void Start()
    {
        gameParams = gameParams.Load();
        lives = gameParams.lives;
        obsContr = GameObject.FindObjectOfType<ObstaclesController>();
        obsContr.changeLaneDelay = gameParams.gap;
        GameController.Instance.GameTime = gameParams.gameTime;
        anim = this.GetComponent<Animator>();
        checkedLanes = new bool[lanes.Length];
        checkedLanes[2] = true;
    }

    // Update is called once per frame
    void Update()
    {
        currentBody = GetTrackedBody();

        TotalLivesShow.Instance.SetLives(lives);
        GameSessionController.Instance.gameSession.finalLives = lives;

        if (firstLaunch)
        {
            if (currentBody!=null)
                    countDown.SetActive(true);
            return;
        }


        if (dead || transition)
            return;

        if (Input.GetKeyDown(KeyCode.A) && current > 0)
        {
            anim.SetTrigger("Left");
            transition = true;
            MusicController.Instance.ChangeLane();

        } else 
        if (Input.GetKeyDown(KeyCode.D) && current < lanes.Length - 1)
        {
            anim.SetTrigger("Right");
            transition = true;
            MusicController.Instance.ChangeLane();

        }
        if (Input.GetKey(KeyCode.W))
            obsContr.speedMultiply =2f;

        if (currentBody!=null)
        {
            KinectPlayerController();
        }
    }

    public void KinectPlayerController()
    {
        float r = Rotation();
        if (r > 0.16)
            r = 1;
        else
            if (r < -0.16)
                r = -1;
            else r = 0;

        if (obsContr != null && obsContr.speedMultiply > 0)
        {
            if (r > 0f && current < lanes.Length - 1 && !transition && previousR != r)
            {
                anim.SetTrigger("Right");
                transition = true;
                MusicController.Instance.ChangeLane();
            }
            else
                if (r < 0f && current > 0 && !transition && previousR != r)
                {
                    anim.SetTrigger("Left");
                    transition = true;
                    MusicController.Instance.ChangeLane();

                }
        }
        if (obsContr != null)
        {
            if (isTiltilng())
            {

                obsContr.speedMultiply += 0.05f;
            }
            else
                obsContr.speedMultiply -= 0.01f;

            obsContr.speedMultiply = Mathf.Clamp(obsContr.speedMultiply, 0f, 2f);
        }
        previousR = r;
    }

    public void EndCountDown()
    {
        firstLaunch = false;
    }

    public void Dead()
    {
        lives--;
        GameSessionController.Instance.gameSession.finalLives = lives;
        TotalLivesShow.Instance.SetLives(lives);
        if (lives <=0)
            Application.Quit();
        for (int i = 0; i < checkedLanes.Length; i++)
            if (checkedLanes[i])
            current = i;
        this.transform.position = lanes[current].position;
        dead = false;
    }

    public void MoveRight()
    {
        current++;
        this.transform.position = lanes[current].position;
        transition = false;
    }

    public void MoveLeft()
    {
        current--;
        this.transform.position = lanes[current].position;
        transition = false;
    }

    public float Rotation()
    {
           //  return (sw.bonePos[0, (int)Kinect.NuiSkeletonPositionIndex.ShoulderLeft] - sw.bonePos[0, (int)Kinect.NuiSkeletonPositionIndex.ShoulderRight]).z;
        Vector3 leftShoulder = CameraSpacePointToVector3(currentBody.Joints[JointType.ShoulderLeft].Position);
        Vector3 rightShoulder = CameraSpacePointToVector3(currentBody.Joints[JointType.ShoulderRight].Position);
        return -(leftShoulder - rightShoulder).z;
    }

    public Vector3 CameraSpacePointToVector3(CameraSpacePoint csp)
    {
        return new Vector3(csp.X, csp.Y, csp.Z);
    }

    public bool isTiltilng()
    {
  /*       Vector3 leftHip = CameraSpacePointToVector3(currentBody.Joints[JointType.HipLeft].Position);
        Vector3 leftKnee = CameraSpacePointToVector3(currentBody.Joints[JointType.KneeLeft].Position);
        Vector3 leftAnkle = CameraSpacePointToVector3(currentBody.Joints[JointType.AnkleLeft].Position);
        Vector3 rightHip = CameraSpacePointToVector3(currentBody.Joints[JointType.HipLeft].Position);
        Vector3 rightAnkle = CameraSpacePointToVector3(currentBody.Joints[JointType.AnkleLeft].Position);

        float LegLength = (leftHip - leftKnee).magnitude;
        LegLength += (leftKnee - leftAnkle).magnitude;
        float ll = (leftHip - leftAnkle).magnitude;
        float lr = (rightHip - rightAnkle).magnitude;
        return (ll / LegLength <= 0.975f && lr / LegLength <= 0.975f);*/
        Vector3 spineBase = CameraSpacePointToVector3(currentBody.Joints[JointType.SpineBase].Position);
        Vector3 spineShoulder = CameraSpacePointToVector3(currentBody.Joints[JointType.SpineShoulder].Position);
        return (spineBase - spineShoulder).z>0f;
    }

    public Body GetTrackedBody()
    {
        Body[] bodies = bodySourceManager.GetData();
        if (bodies == null)
            return null;

        foreach (Body b in bodySourceManager.GetData())
        {
            if (b.IsTracked)
                return b;
        }
        return null;
    }
   
    void OnTriggerEnter(Collider col)
    {
        if (!transition)
        {
            obsContr.speedMultiply = 0f;
            anim.SetTrigger("Dead");
            dead = true;
            fc.Failure(1f);
            MusicController.Instance.Fall();

        }
    }
}
