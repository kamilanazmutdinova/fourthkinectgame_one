﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {
    public int id;
    public void Move(Vector3 delta)
    {
        this.transform.position += delta;
    }
}
