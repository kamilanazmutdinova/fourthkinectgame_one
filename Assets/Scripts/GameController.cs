﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    private static GameController instance_;
    public GameObject quitDialog;

    public static GameController Instance
    {
        get
        {
            instance_ = GameObject.FindObjectOfType<GameController>();
            return instance_;
        }
    }

    public float GameTime = 600f;
    bool countDowning = false;
  
    public void Update()
    {
        if (countDowning)
            GameTime -= Time.deltaTime;

        TotalTimeShow.Instance.SetTime((int)GameTime);
        if (GameTime <= 0f)
            Quit();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            quitDialog.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void StartCountDown()
    {
        countDowning = true;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void CancelQuitDialog()
    {
        quitDialog.SetActive(false);
        Time.timeScale = 1f;
    }
}
