﻿using UnityEngine;
using System.Collections;

public class ObstaclesController : MonoBehaviour {
    public int laneNum = 5;
    public float width;
    public float dt = 0.9f;
    float timer = 0.5f;
    public float speed = 5f;
    public float speedMultiply = 1f;
    public float changeLaneDelay = 0.7f;
    PrefabsPool prefabsPool;
    int currState = 2;
    int nextState = 2;
    int prevState = 2;
    int prePrev = 2;
    public float laneChangeTime = 5f;
    float laneTimer = 5f;
    public float score;
    enum States { 
       Stay, Transition, TransitionEnd  
    }

    States state = States.Stay;
    void Start () {
        prefabsPool = PrefabsPool.Instance;
        timer = dt;
        
	}
    public int GetCurrentLane()
    {
        return prePrev;
    }

    void Update () {
       timer -= Time.deltaTime * speedMultiply;
       ChangeLane();
       SpawnNew();
       score += Time.deltaTime * speedMultiply * speed;
        TotalScoreShow.Instance.SetScore((int)score);
        GameSessionController.Instance.gameSession.finalScore = (int)score;
        foreach (Obstacle obstacle in prefabsPool.obstacles)
                obstacle.Move(Vector3.back * Time.deltaTime*speedMultiply* speed);
    }

    public int GetScore()
    {
        return (int)score;
    }
    
    public void SpawnNew()
    {
        if (timer < 0)
        {
            Vector3 pos = this.transform.position;
            pos.x -= width / 2f;
            for (int i = 0; i < laneNum; i++)
            {
                if (i != currState && i!= nextState)
                {
                    GameObject obj = prefabsPool.GetFromPool(prefabsPool.GetRandomId());
                    obj.transform.position = pos;
                }
                pos.x += width / (float)(laneNum-1);
            }
            timer = dt;
        }
    }

    public void ChangeLane()
    { 
        switch(state)
        {
            case States.Stay:
                laneTimer -= Time.deltaTime * speedMultiply;
                prePrev = prevState;
                if (laneTimer < 0)
                {
                    laneTimer = laneChangeTime * changeLaneDelay;
                    state = States.Transition;
                    int rand = Random.Range(0,2);
                    if (rand == 0)
                        nextState = currState - 1;
                    else nextState = currState + 1;
                    if (nextState == laneNum)
                        nextState -= 2;
                    if (nextState == -1)
                        nextState += 2;
                    nextState = Mathf.Clamp(nextState, 0, laneNum -1);
                }
                break;
            case States.Transition:
                laneTimer -= Time.deltaTime * speedMultiply;
                if (laneTimer < 0)
                {
                    laneTimer = laneChangeTime;
                    state = States.Stay;
                    prePrev = prevState;
                    prevState = currState;
                    currState = nextState;
                }
                break;
            case States.TransitionEnd:
                break;
        }
    }

}
