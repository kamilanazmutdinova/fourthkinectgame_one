﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using System;
[Serializable]
[XmlRoot("Session")]
public class GameSession {
    [XmlAttribute("BuildVersion")]
    public string buildVersion = "1.01";
    [XmlAttribute("PatientId")]
    public int patientId=0;
    [XmlAttribute("BeginningTime")]
    public string beginningTime;
    [XmlAttribute("EndingTime")]
    public string endingTime;
    [XmlAttribute("FinalScore")]
    public int finalScore=0;
    [XmlAttribute("FinalLives")]
    public int finalLives=0;
 
    public GameSession(){ 
    }

    public GameSession(int id)
    {
        this.patientId = id;
    }


    public void SetFinalScore(int score)
    {
        this.finalScore = score;
    }

    public void Save(string name)
    {
        String path = Application.dataPath + "/../out/" + name + ".xml";
        Debug.Log(path);
        XmlSerializer serializer = new XmlSerializer(typeof(GameSession));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public GameSession Load(string name)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(GameSession));
        try
        {
            Debug.Log(Application.persistentDataPath);
            using (var stream = new FileStream(Application.persistentDataPath + "/"+name+".xml", FileMode.Open))
            {
                return serializer.Deserialize(stream) as GameSession;
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            return new GameSession();
        }
    }

}
