﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TotalLivesShow : MonoBehaviour {
    private static TotalLivesShow instance_;
    public static TotalLivesShow Instance
    {
        get
        {
            instance_ = GameObject.FindObjectOfType<TotalLivesShow>();
            return instance_;
        }
    }

    Text text;
    void Start()
    {
        text = GetComponent<Text>();
    }

    public void SetLives(int lives)
    {
        text.text = "Lives: " + lives;
    }
}
